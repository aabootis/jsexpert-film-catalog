import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FilmService } from '../film.service';

@Component({
  selector: '.films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.scss']
})
export class FilmsComponent implements OnInit {

  public films;

  constructor(private filmsService: FilmService) {}

  ngOnInit() {
    this.films = this.filmsService.films;
  }

}
